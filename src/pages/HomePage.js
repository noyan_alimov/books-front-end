import React from 'react';

const HomePage = () => {
  return (
    <div>
      <h1>This is a basic CRUD web app</h1>
      <h1>Do these operations to store books</h1>
    </div>
  );
};

export default HomePage;
