import React, { useState } from 'react';

const DeletePage = () => {
  const [book, setBook] = useState('');

  const { id } = book;

  const onChange = e => setBook(e.target.value);

  const onSubmit = async e => {
    e.preventDefault();
    await fetch(`https://localhost:44378/api/books/${id}`, {
      method: 'DELETE'
    });
  };

  return (
    <form onSubmit={onSubmit}>
      <input
        type="text"
        name="id"
        value={id}
        onChange={onChange}
        placeholder="Enter the ID of the book you want to delete..."
        required
      />
      <input type="submit" value="SUBMIT" />
    </form>
  );
};

export default DeletePage;
