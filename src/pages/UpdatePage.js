import React, { useState } from 'react';

const UpdatePage = () => {
  const [book, setBook] = useState({
    id: '',
    name: '',
    price: '',
    category: '',
    author: ''
  });

  const { id, name, price, category, author } = book;

  const onChange = e => setBook({ ...book, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    const res = await fetch(`https://localhost:44378/api/books/${id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        name,
        price,
        category,
        author
      })
    });
    const data = res.json();
    console.log(data);
  };

  return (
    <form onSubmit={onSubmit}>
      <input
        type="text"
        name="id"
        value={id}
        onChange={onChange}
        placeholder="Enter the ID of the book you want to update"
        required
      />
      <input
        type="text"
        name="name"
        value={name}
        onChange={onChange}
        placeholder="Enter the name of the book..."
        required
      />
      <input
        type="number"
        name="price"
        value={price}
        onChange={onChange}
        placeholder="Price..."
        required
      />
      <input
        type="text"
        name="category"
        value={category}
        onChange={onChange}
        placeholder="Category..."
        required
      />
      <input
        type="text"
        name="author"
        value={author}
        onChange={onChange}
        placeholder="Author..."
        required
      />
      <input type="submit" value="SUBMIT" />
    </form>
  );
};

export default UpdatePage;
