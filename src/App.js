import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import AddPage from './pages/AddPage';
import ViewPage from './pages/ViewPage';
import UpdatePage from './pages/UpdatePage';
import DeletePage from './pages/DeletePage';

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/add" component={AddPage} />
        <Route path="/view" component={ViewPage} />
        <Route path="/update" component={UpdatePage} />
        <Route path="/delete" component={DeletePage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
